import React from "react";
import {
    Card,
    Grid
} from "@mui/material";
import { TaskList } from "./TaskList";


export const TaskDashboard = ({tasks, onToggle, onDelete}) => {
    return (
        <Grid container justifyContent="center" sx={{ marginTop: 4 }}>
            <Grid item xs={12}>
                <Card sx={{ padding: 1 }} raised={true}>
                    <TaskList tasks={tasks} onToggle={onToggle} onDelete={onDelete}/>
                </Card>
            </Grid>
        </Grid>
    );
};
