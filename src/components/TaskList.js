import React, { useState } from "react";
import {
    FormControl,
    Grid,
    InputLabel,
    List,
    MenuItem,
    Select,
    TextField,
} from "@mui/material";
import { TaskItem } from "./TaskItem";

export const TaskList = ({ tasks, onToggle, onDelete }) => {

    // Searching Task
    const [searchedTask, setSearchedTask] = React.useState("");

    // Filtering Task
    const [selectedOrder, setSelectedOrder] = useState('');
    const handleOrderChange = (newValue) => {
        setSelectedOrder(newValue.target.value);
    }

    const [isFiltered, setIsFiltered] = useState(false);
    const handleIsFiltered = () =>  {
        setIsFiltered(true);
    }

    const priorityOrder = ["high", "medium", "low"]; // Defining Priority Order

    return (
        <>
            <Grid container spacing={2} alignItems="center">
                <Grid item xs={8}>
                    <form>
                        <TextField
                            id="task-input"
                            label="Search your task"
                            value={searchedTask}
                            fullWidth
                            onChange={(evt) =>
                                setSearchedTask(evt.target.value)
                            }
                        />
                    </form>
                </Grid>
                <Grid item xs={4}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">
                            Filter
                        </InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={selectedOrder}
                            label="Age"
                            onChange={handleOrderChange}
                        >
                            <MenuItem value={'Ascending'} onClick={handleIsFiltered}>Ascending Priority</MenuItem>
                            <MenuItem value={'Descending'} onClick={handleIsFiltered}>Descending Priority</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>

            <List sx={{ padding: 1 }}>
                {tasks
                    .filter((task) => {
                        let taskValue = task.value.toLowerCase();
                        return searchedTask.toLowerCase() === ""
                            ? task
                            : taskValue.includes(searchedTask);
                    }).sort((taskA, taskB) => {
                        if(isFiltered && selectedOrder.includes('Ascending')) {
                            const priorityAIndex = priorityOrder.indexOf(taskA.priority.toLowerCase());
                            const priorityBIndex = priorityOrder.indexOf(taskB.priority.toLowerCase());
                            return priorityBIndex - priorityAIndex;
                        } else if(isFiltered && selectedOrder.includes("Descending")) {
                            const priorityAIndex = priorityOrder.indexOf(taskA.priority.toLowerCase());
                            const priorityBIndex = priorityOrder.indexOf(taskB.priority.toLowerCase());
                            return priorityAIndex - priorityBIndex;
                        }
                    })
                    .map((task) => (
                        <TaskItem
                            key={task.id}
                            task={task}
                            onToggle={onToggle}
                            onDelete={onDelete}
                        />
                    ))}
            </List>
        </>
    );
};