import React from "react";
import {
    Button,
    Card,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Select,
    TextField,
} from "@mui/material";
import DataSaverOnOutlinedIcon from "@mui/icons-material/DataSaverOnOutlined";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";


export const TaskForm = ({ taskRef, onClear, onSubmit, selectedPriority, onPriorityChange}) => {
    // function handleChange(evt) {
    //     onChange(evt.target.value);
    // }
    return (
        <Grid container justifyContent="center">
            <Grid item xs={12}>
                <Card sx={{ padding: 4 }} raised={true}>
                    <Grid container spacing={2} alignItems="center">
                        <Grid item xs={6}>
                            <form onSubmit={onSubmit}>
                                <TextField
                                inputRef={taskRef}
                                    id="task-input"
                                    label="Insert your task"
                                    // value={task}
                                    fullWidth
                                    // onChange={handleChange}
                                />
                            {/* <input type="text"
                                ref={taskRef}
                                    id="task-input"
                                    label="Insert your task"
                                    // value={task}
                                    fullWidth
                                    // onChange={handleChange}
                            /> */}
                            </form>
                        </Grid>
                        <Grid item xs={2}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">
                                    Priority
                                </InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={selectedPriority}
                                    label="Age"
                                    onChange={onPriorityChange}
                                >
                                    <MenuItem value={'Low'} >Low</MenuItem>
                                    <MenuItem value={'Medium'} >Medium</MenuItem>
                                    <MenuItem value={'High'} >High</MenuItem>
                                </Select>
                            </FormControl>
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                variant="contained"
                                color="primary"
                                fullWidth
                                onClick={onSubmit}
                            >
                                <DataSaverOnOutlinedIcon /> Submit
                            </Button>
                        </Grid>
                        <Grid item xs={2}>
                            <Button
                                variant="contained"
                                color="secondary"
                                fullWidth
                                onClick={onClear}
                            >
                                <DeleteOutlinedIcon /> Clear
                            </Button>
                        </Grid>
                    </Grid>
                </Card>
            </Grid>
        </Grid>
    );
};
