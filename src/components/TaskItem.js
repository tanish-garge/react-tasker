import React from "react";
import {
    Button,
    Checkbox,
    Chip,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
} from "@mui/material";
import DeleteForeverOutlinedIcon from "@mui/icons-material/DeleteForeverOutlined";
import { lime } from '@mui/material/colors';
import { deepOrange } from '@mui/material/colors';

export const TaskItem = ({task, onToggle, onDelete}) => {

    const highOrange =   deepOrange[900];
    const textColor =   lime[100];
    const mediumOrange =   deepOrange[400];
    const lowOrange =   deepOrange[200];

    const textDecorationStyle = task.isCompleted ? {textDecoration: "line-through"} : {};

    let chipElement;
    let priorityChipElement;

    if(task.isCompleted) {
        chipElement = <Chip label="Completed" variant="outlined" color="success" sx={{margin: 5}}/>
    } else {
        chipElement = <Chip label="pending" variant="outlined" color="warning" sx={{margin: 5}}/>
    }

    if(task.priority === 'Low') {
        priorityChipElement = <Chip label="Low" sx={{background: lowOrange, color: textColor}}/>
    } else if (task.priority === 'Medium') {
        priorityChipElement = <Chip label="Medium" sx={{background: mediumOrange, color: textColor}}/>
    } else if (task.priority === 'High') {
        priorityChipElement = <Chip label="High" sx={{background: highOrange, color: textColor}}/>
    }
    
    return (
        <ListItem
            sx={{
                paddingBottom: 2,
                paddingTop: 2,
                borderBottom: "solid 1px #AAA",
            }}
            disablePadding
        >
            <Checkbox checked={task.isCompleted} onChange={() => onToggle(task.id)}/>

            <ListItemText
                primary={task.value}
                sx={textDecorationStyle}
            />

            <ListItemSecondaryAction>
                {priorityChipElement}
                {chipElement}
                <Button
                    variant="contained" 
                    color="error"
                    sx={{ marginLeft: 5 }}
                    onClick={() => onDelete(task.id)}
                >
                    <DeleteForeverOutlinedIcon /> Delete
                </Button>
            </ListItemSecondaryAction>
            
        </ListItem>
    );
};
