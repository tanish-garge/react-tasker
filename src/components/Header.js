import React from "react";
import { AppBar, Container, Toolbar, Typography } from "@mui/material";
import Logo from '../tasker-logo.png';


export const Header = () => {
    return (
        <AppBar position="static" sx={{ background: "#333" }}>
            <Container maxWidth="lg">
                <Toolbar sx={{ paddingLeft: "0!important" }}>
                    <Typography
                        sx={{ paddingLeft: "0" }}
                        variant="h4"
                        component="div"
                    >
                        <img
                            src={Logo}
                            alt="Tasker Logo"
                            style={{ height: "90px" }}
                        />
                    </Typography>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
