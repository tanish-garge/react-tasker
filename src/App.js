import React, { useRef, useState } from 'react';
import {v4 as uuidv4} from 'uuid';

import {Container } from '@mui/material';
import { Header } from './components/Header';
import { TaskForm } from './components/TaskForm';
import { TaskDashboard } from './components/TaskDashboard';
// import { Priority } from './components/Priority';



const App = () => {
    const [tasks, setTasks] = useState([]);
    const taskRef = useRef();
    // const [task, setTask] = useState('');  // Controlled Input
    const [selectedPriority, setSelectedPriority] = useState('Medium');


    const handlePriorityChange = (newValue) => {
        setSelectedPriority(newValue.target.value);
        // console.log(newValue.target.value);
    }

    function clearTask() {
        // setTask('');
        taskRef.current.value = '';
    }

    function submitTask(evt) {
        evt.preventDefault();
        if(taskRef.current.value === '') {
            alert("Blank Input is not allowed");
        } else {
            setTasks(tasks.concat({id: uuidv4(), value: taskRef.current.value, isCompleted: false, priority:selectedPriority}));
            // setTask('');
            taskRef.current.value = '';
        }
        // console.log(priority);
    }

    function handleToggleCompletion(taskId) {
        // const updatedTasks = tasks.map(task => {
        //     if(task.id === taskId) {
        //         return {
        //             ...task,
        //             isCompleted: !task.isCompleted
        //         };
        //     }
        //     return task;
        // });
        // setTasks(updatedTasks);
        setTasks(tasks.map(task => task.id === taskId ? {...task, isCompleted: !task.isCompleted } : task));
    }

    function handleDelete(taskId) {
        setTasks(tasks.filter(task => task.id !== taskId));
    }
    return (
        <div>
            <Header/>
            <Container maxWidth='lg' sx={{ marginTop: 4 }}>
                <TaskForm taskRef={taskRef} onClear={clearTask} onSubmit={submitTask} selectedPriority={selectedPriority} onPriorityChange={handlePriorityChange}/>
                {/* <Priority selectedPriority = {selectedPriority} onPriorityChange={setSelectedPriority}/> */}
                <TaskDashboard tasks={tasks} onToggle={handleToggleCompletion} onDelete={handleDelete}/>
            </Container>
        </div>
    );
};

export default App;
